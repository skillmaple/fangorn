export class Tree {
    treeId: string;
    organization: string;
    name: string;
    root: string;
    createdBy:  string;
}