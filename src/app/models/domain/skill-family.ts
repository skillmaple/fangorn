import {Skill} from './skill';
import {Tree} from './tree';


export class SkillFamily {
    tree: Tree;
    skill: Skill;
    parents: Skill[];
    children: Skill[];
}