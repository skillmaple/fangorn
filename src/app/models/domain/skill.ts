export class Skill {
    skillId: string;
    treeId: string;
    name: string;
    description: string;
    background: string;
    numUsers: number;
    icon: string;
    createdBy: string;
}