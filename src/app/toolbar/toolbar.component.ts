import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  // Menu Navigation

  goToMenuOption(path: string, menu: any) {
    this.router.navigateByUrl(path);
    menu.close();
  }

  
  goToSkillMaple() {
    let hostName = window.location.hostname;
    window.location.href = `https://${hostName}/app`;
  }

  goToSignOut() {
    let hostName = window.location.hostname;
    window.location.href = `https://${hostName}/app/logout`;
  }


}
