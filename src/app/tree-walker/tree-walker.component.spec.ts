import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeWalkerComponent } from './tree-walker.component';

describe('TreeWalkerComponent', () => {
  let component: TreeWalkerComponent;
  let fixture: ComponentFixture<TreeWalkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeWalkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeWalkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
