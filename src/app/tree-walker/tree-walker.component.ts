import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import {SkillService} from '../services/skill.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SkillFamily } from '../models/domain/skill-family';
import { Skill } from '../models/domain/skill';
import { AddParentDialogComponent } from '../dialogs/add-parent-dialog/add-parent-dialog.component';
import { AddSkillDialogComponent } from '../dialogs/add-skill-dialog/add-skill-dialog.component';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-tree-walker',
  templateUrl: './tree-walker.component.html',
  styleUrls: ['./tree-walker.component.css'],
  encapsulation: ViewEncapsulation.None
})
@Injectable()
export class TreeWalkerComponent implements OnInit {
  loading: boolean = false;
  mode: string = 'View';
  family: SkillFamily;
  skillImage: string;

  constructor(
    private skillService:SkillService,
    private route: ActivatedRoute, 
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    let skillId = this.route.snapshot.params['skillId'];
    this.getSkillFamily(skillId);
  }

  getSkillFamily(skillId: string) {
    this.loading = true;
    this.skillService.getSkillFamily(skillId).subscribe(
      (response) => {
        this.family = response;
        this.skillImage = this.family.skill.icon != null && this.family.skill.icon !== "" ? this.family.skill.icon : "default";
        this.loading = false;
      },
      (error) => {
        console.error(error); // TODO: Log error
        this.loading = false;
      }
    );
  }

  selectMode(mode: string) {
    this.mode = mode;
  }

  addSubskill() {
    const dialogRef = this.dialog.open(AddSkillDialogComponent, { data: {} });

    dialogRef.afterClosed().subscribe(result => {
      if (result != null && result !== "") {
        this.skillService.createSubskill(this.family.skill.skillId, result).subscribe(
          (response) => {
            this.loading = false;
            if (!response.ok) {
              // TODO: Log the error
              alert("An error occurred while trying to add the subskill");
            } else {
              this.getSkillFamily(this.family.skill.skillId);
            }
          },
          (error) => {
            this.loading = false;
            console.error(error); // TODO: Log error
            alert("An error occurred while trying to add the subskill");
          }
        )
      }
    });
  }

  navigateToSkill(skill: Skill) {
    this.router.navigateByUrl(`/in/explore/${skill.skillId}`);
    this.getSkillFamily(skill.skillId);
  }

  addSuperskill() {
    const dialogRef = this.dialog.open(AddParentDialogComponent, {});
    
    dialogRef.afterClosed().subscribe(result => {
      if (result != null && result !== "") {
        this.skillService.addParent(this.family.skill.skillId, result).subscribe(
          (response) => {
            this.loading = false;
            this.getSkillFamily(this.family.skill.skillId);
          },
          (err: HttpErrorResponse) => {
            this.loading = false;
            if (err.error instanceof Error) {
              // TODO: Log the error
              alert(`An error occurred: ${err.error.message}`);
            } else {
              // TODO: Log the error
              if (err.status === 409) {
                alert("Can't add parent due to conflict");
              } else {
                alert("Error occurred while trying to add the parent skill");
              }
            }
          }
        )
      }
    });
  }

  editSkill() {
    this.router.navigateByUrl(`/in/skills/${this.family.skill.skillId}/edit`);
  }

  deleteSkill() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete skill',
        message: `Are you sure you want to delete this skill?`,
        confirmLabel: 'Yes',
        denyLabel: 'No thanks'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.skillService.deleteSkill(this.family.skill.skillId).subscribe(
          (response) => {
            this.loading = false;
            this.router.navigateByUrl(`/in/explore/${this.family.tree.root}`);
            this.getSkillFamily(this.family.tree.root);
          },
          (err: HttpErrorResponse) => {
            this.loading = false;
            if (err.error instanceof Error) {
              alert(`An error occurred: ${err.error.message}`);
            } else {
              if (err.status === 409) {
                alert("Can't delete skill with children");
              } else {
                alert("Error occurred while trying to delete skill");
              }
            }
          }
        )
      }
    });
  }

  deleteParent(event: Event, parentSkill: Skill) {
    event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Remove parent skill',
        message: `Are you sure you want to remove this parent skill relationship?`,
        confirmLabel: 'Yes',
        denyLabel: 'No thanks'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.skillService.removeParent(this.family.skill.skillId, parentSkill.skillId).subscribe(
          (response) => {
            this.loading = false;
            this.getSkillFamily(this.family.skill.skillId);
          },
          (err: HttpErrorResponse) => {
            this.loading = false;
            if (err.error instanceof Error) {
              // TODO: Log the error
              alert(`An error occurred: ${err.error.message}`);
            } else {
              // TODO: Log the error
              if (err.status === 409) {
                alert("Can't remove parent since it would cause the skill to have no parents");
              } else {
                alert("Error occurred while trying to remove the parent skill");
              }
            }
          }
        )
      }
    });
  }

}

