import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Injectable} from '@angular/core';
import {AuthService} from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private authService: AuthService, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        var isAuthenticated = this.authService.isAuthenticated();
        if (isAuthenticated === false) {
            let hostName = window.location.hostname;
            window.location.href = `https://${hostName}/app/login`;
            return false;
        }
        return true; // Logged in so return true;
    }
}