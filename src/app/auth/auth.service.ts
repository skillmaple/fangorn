import { ConfigService } from '../api/config.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import * as auth0 from 'auth0-js';

@Injectable()
export class AuthService {
 
  auth0;
  
  constructor(public router: Router, private configService: ConfigService) {}

  private configureAuth0(data): void {
    let hostName = window.location.hostname;
    this.auth0 = new auth0.WebAuth({
      clientID: data.AUTH0_CLIENT_ID,
      domain: data.AUTH0_DOMAIN,
      responseType: 'token id_token',
      audience: data.AUTH0_AUDIENCE,
      redirectUri: `https://${hostName}/app/auth`,      
      scope: 'openid'
    });
  }

  public login(): void {
    if (this.auth0 == null) {
      this.configService.getInitConfig().subscribe(
        (response) => {
          this.configureAuth0(response);
          this.auth0.authorize();
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.auth0.authorize();
    }
    
  }

  public handleAuthentication(): void {
    if (this.auth0 == null) {
      this.configService.getInitConfig().subscribe(
        (response) => {
          this.configureAuth0(response);
          this.processAuthenticationResults();
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      this.processAuthenticationResults();
    }
  }

  private processAuthenticationResults() : void {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate(['/in']);
      } else if (err) {
        let hostName = window.location.hostname;
        window.location.href = `https://${hostName}/app/login`;
        console.log(err);
      }
    });
  }

  private setSession(authResult): void {
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);
  }

  public logout(): void {
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    // Go back to the home route
    this.router.navigate(['/']);
  }

  public isAuthenticated(): boolean {
    // Check whether the current time is past the
    // access token's expiry time
    var expirationString = localStorage.getItem('expires_at');
    if (expirationString == null || expirationString === '') {
        return false;
    }
    const expireTime = JSON.parse(expirationString);
    return new Date().getTime() < expireTime;
  }
}