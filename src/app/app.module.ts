import { HttpModule } from '@angular/http';
import { ConfigService } from './api/config.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatTabsModule} from '@angular/material/tabs';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppContext } from './app-context';
import { AuthGuard } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { BottomToolbarComponent } from './bottom-toolbar/bottom-toolbar.component';
import { TreeManagerComponent } from './tree-manager/tree-manager.component';
import { TreeService } from './services/tree.service';
import { SkillService }  from './services/skill.service';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { TreeFormComponent } from './tree-manager/tree-form/tree-form.component';
import { TreeWalkerComponent } from './tree-walker/tree-walker.component';
import { NotificationDialogComponent } from './dialogs/notification-dialog/notification-dialog.component';
import { AddSkillDialogComponent } from './dialogs/add-skill-dialog/add-skill-dialog.component';
import { AddParentDialogComponent } from './dialogs/add-parent-dialog/add-parent-dialog.component';
import { SkillEditorComponent } from './skill-editor/skill-editor.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    BottomToolbarComponent,
    TreeManagerComponent,
    ConfirmDialogComponent,
    TreeFormComponent,
    TreeWalkerComponent,
    NotificationDialogComponent,
    AddSkillDialogComponent,
    AddParentDialogComponent,
    SkillEditorComponent
  ],
  entryComponents: [
    ConfirmDialogComponent,
    NotificationDialogComponent,
    AddSkillDialogComponent,
    AddParentDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatCardModule,
    MatProgressSpinnerModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [AuthService, AuthGuard, ConfigService, AppContext, TreeService, SkillService],
  bootstrap: [AppComponent]
})
export class AppModule { }
