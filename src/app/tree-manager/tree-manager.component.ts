import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AppContext} from '../app-context'
import { Injectable } from '@angular/core';
import { Tree } from '../models/domain/tree';
import { TreeService } from '../services/tree.service';
import { ConfirmDialogComponent } from '../dialogs/confirm-dialog/confirm-dialog.component';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tree-manager',
  templateUrl: './tree-manager.component.html',
  styleUrls: ['./tree-manager.component.css'],
  encapsulation: ViewEncapsulation.None
})
@Injectable()
export class TreeManagerComponent implements OnInit {
  public orgId: string;
  public trees: Tree[];
  public loading: boolean;

  constructor(
    private app: AppContext, 
    private treeService: TreeService, 
    private router: Router, 
    public dialog: MatDialog
  ) { 
    this.orgId = app.OrganizationId;
    this.loading = true;
  }

  ngOnInit() {
    this.getTrees();
  }

  selectTree(tree: Tree) {
    this.router.navigateByUrl(`/in/explore/${tree.root}`);
  }

  confirmDelete($event: Event, tree: Tree) {
    $event.stopPropagation();
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: {
        title: 'Delete skill tree',
        message: `Are you sure you want to delete the ${tree.name} skill tree and all of its skills?`,
        confirmLabel: 'Yes',
        denyLabel: 'No thanks'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.deleteTree(tree.treeId);
      }
    });
  }

  getTrees() {
    this.treeService.getOrganizationTrees(this.orgId).subscribe(
      (response) => {
        this.trees = response;
        this.loading = false;
      },
      (error) => {
        console.error(error); // TODO: Log error
        this.loading = false;
      }
    );
  }

  addTree() {
    this.router.navigateByUrl('/in/trees/add');
  }

  editTree($event: Event, tree: Tree) {
    event.stopPropagation();
    this.router.navigateByUrl(`/in/trees/edit/${tree.treeId}`);
  }

  deleteTree(treeId: string) {
    this.loading = true;
    this.treeService.deleteTree(treeId).subscribe(
      (response) => {
        this.loading = false;
        if (response.ok) {
          alert("Deleted the skill"); // TODO: Replace with notification dialog
          this.getTrees();
        } else {
          alert("Failed to delete the skill"); // TODO: Replace with notification dialog
        }
      },
      (error) => {
        this.loading = false;
        console.error(error); // TODO: Log error
        alert("Failed to delete the skill"); // TODO: Replace with notification dialog
      }
    )
  } 

}
