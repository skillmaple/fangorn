import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TreeService } from '../../services/tree.service';

@Component({
  selector: 'app-tree-form',
  templateUrl: './tree-form.component.html',
  styleUrls: ['./tree-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
@Injectable()
export class TreeFormComponent implements OnInit {
  private treeId: string;
  private isNewTree: boolean;
  public treeForm: FormGroup;
  public loading: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private treeService: TreeService
  ) { }

  ngOnInit() {
    this.loading = true;
    this.treeId = this.route.snapshot.params['treeId'];
    this.isNewTree = (this.treeId == null || this.treeId === "");
    this.setupForm("");
    if (this.isNewTree === false) {
      this.treeService.getTree(this.treeId).subscribe(
        (response) => {
          this.loading = false;
          this.treeForm.setValue({
            treeName: response.name
          });
        },
        (error) => {
          
          console.error(error); // TODO: Log error
          alert("Failed to retrieve skill to update");
          this.cancelButton();
          this.loading = false;
        }
      );
    } else {
      this.loading = false;
    }
    
  }

  setupForm(name: string) {
    this.treeForm = new FormGroup({
      'treeName': new FormControl(name, [Validators.required, this.maxLength])
    });
  }

  submitTree() {
    let treeName = this.treeForm.value.treeName;
    if (this.treeForm.valid) {
      if (this.isNewTree) {
        this.createTree(treeName);
      } else {
        this.updateTree(this.treeId, treeName);
      }
    }
  }

  createTree(name: string) {
    this.loading = true;
    this.treeService.createTree(name).subscribe(
      (response) => {
        this.loading = false;
        if (response.ok) {
          alert("Added the skill"); // TODO: Replace with notification dialog
          this.router.navigateByUrl('/in/trees');
        } else {
          alert("Failed to add the skill"); // TODO: Replace with notification dialog
        }
      },
      (error) => {
        this.loading = false;
        console.error(error); // TODO: Log error
        alert("Failed to add the skill"); // TODO: Replace with notification dialog
      }
    )
  }

  updateTree(id: string, name: string) {
    this.loading = true;
    this.treeService.updateTree(id, name).subscribe(
      (response) => {
        this.loading = false;
        if (response.ok) {
          alert("Updated the skill"); // TODO: Replace with notification dialog
          this.router.navigateByUrl('/in/trees');
        } else {
          alert("Failed to update the skill"); // TODO: Replace with notification dialog
        }
      },
      (error) => {
        this.loading = false;
        console.error(error); // TODO: Log error
        alert("Failed to update the skill"); // TODO: Replace with notification dialog
      }
    )
  }

  maxLength(control: FormControl): {[s: string]: boolean} {
    if (control.value != null && control.value.length > 30) {
      return {'maxLength': true};
    }
    return null;
  }

  cancelButton() {
    this.router.navigateByUrl('/in/trees');
  }

}
