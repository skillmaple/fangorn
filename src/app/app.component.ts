import { Component } from '@angular/core';
import { AppContext} from './app-context';
import { Injectable } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
@Injectable()
export class AppComponent {

  constructor(private app: AppContext) {
    this.app.OrganizationId = "00000000-0000-0000-0000-000000000000";
  }
}
