import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bottom-toolbar',
  templateUrl: './bottom-toolbar.component.html',
  styleUrls: ['./bottom-toolbar.component.css']
})
export class BottomToolbarComponent implements OnInit {
  @Input() navLinks: any[];
  @Input() showActive: boolean;

  constructor() { }
  
  ngOnInit() {
  }

}
