import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import {SkillService} from '../services/skill.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Skill } from '../models/domain/skill';
import { NotificationDialogComponent } from '../dialogs/notification-dialog/notification-dialog.component';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-skill-editor',
  templateUrl: './skill-editor.component.html',
  styleUrls: ['./skill-editor.component.css'],
  encapsulation: ViewEncapsulation.None
})
@Injectable()
export class SkillEditorComponent implements OnInit {

  public loading: boolean = false;
  public skill: Skill;
  public skillForm: FormGroup;

  constructor(
    private skillService:SkillService,
    private route: ActivatedRoute, 
    private router: Router,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    let skillId = this.route.snapshot.params['skillId'];
    this.getSkill(skillId);
  }

  getSkill(skillId: string) {
    this.loading = true;
    this.skillService.getSkill(skillId).subscribe(
      (response) => {
        this.skill = response;
        this.setupForm(this.skill);
        this.loading = false;
      },
      (error) => {
        console.error(error); // TODO: Log error
        this.loading = false;
      }
    );
  }

  setupForm(skill: Skill) {
    this.skillForm = new FormGroup({
      'skillName': new FormControl(skill.name, [Validators.required, this.maxLength(80)]),
      'skillIcon': new FormControl(skill.icon, []),
      'skillDescription': new FormControl(skill.description, [this.maxLength(500)]),
      'skillBackground': new FormControl(skill.background, [this.maxLength(5000)])
    });
  }

  maxLength(length: number): (FormControl) => {[s: string]: boolean} {
    return (control: FormControl) => {
      if (control.value != null && control.value.length > length) {
        return {'maxLength': true};
      }
      return null;
    }
  }

  cancel() {
    this.router.navigateByUrl(`/in/explore/${this.skill.skillId}`);
  }

  updateFailureMessage() {
    this.dialog.open(NotificationDialogComponent, {
      data: {
        title: 'Error',
        message: `An error occurred while trying to update the skill.`
      }
    });
  }

  updateSuccessMessage() {
    this.dialog.open(NotificationDialogComponent, {
      data: {
        title: 'Success',
        message: `The skill was successfully updated.`
      }
    }).afterClosed().subscribe(result => {
      this.router.navigateByUrl(`/in/explore/${this.skill.skillId}`);
    });
  }

  submit() {
    if (this.skillForm.valid) {
      this.loading = true;
      this.skill.name = this.skillForm.value.skillName;
      this.skill.icon = this.skillForm.value.skillIcon;
      this.skill.description = this.skillForm.value.skillDescription;
      this.skill.background = this.skillForm.value.skillBackground;

      this.skillService.updateSkill(this.skill).subscribe(
        (response) => {
          this.loading = false;
          if (response.ok) {
            this.updateSuccessMessage();
          } else {
            this.updateFailureMessage();
          }
        },
        (error) => {
          this.loading = false;
          console.error(error); // TODO: Log error
          this.updateFailureMessage();
        }
      )
    }
  }

}
