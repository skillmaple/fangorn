import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; 
import { AuthGuard } from './auth/auth-guard.service';
import { ToolbarComponent } from './toolbar/toolbar.component';

import {TreeManagerComponent} from './tree-manager/tree-manager.component';
import {TreeFormComponent} from './tree-manager/tree-form/tree-form.component';
import {TreeWalkerComponent} from './tree-walker/tree-walker.component';
import {SkillEditorComponent} from './skill-editor/skill-editor.component';


const appRoutes: Routes = [
    { path: '', redirectTo: '/in/trees', pathMatch: 'full'},
    { path: 'in', component: ToolbarComponent, canActivate: [AuthGuard], children: [
        { path: '', redirectTo: '/trees', pathMatch: 'full'},
        { path: 'trees', component: TreeManagerComponent},
        { path: 'trees/add', component: TreeFormComponent},
        { path: 'trees/edit/:treeId', component: TreeFormComponent },
        { path: 'explore/:skillId', component: TreeWalkerComponent },
        { path: 'skills/:skillId/edit', component: SkillEditorComponent}
    ]}
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}