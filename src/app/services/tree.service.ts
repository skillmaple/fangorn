import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';
import { Tree } from '../models/domain/tree';

@Injectable()
export class TreeService {
    baseUrl: string;
    
    constructor(private http:Http) {
        this.baseUrl = `https://${window.location.hostname}/api/v1`;
    }

    getOrganizationTrees(orgId: string): Observable<Tree[]> {
        return this.http.get(`${this.baseUrl}/trees?orgId=${orgId}`)
            .map(
                (response: Response) => {
                    const data = response.json() as Tree[];
                    return data;
                }
            );
    }

    getTree(treeId: string): Observable<Tree> {
        return this.http.get(`${this.baseUrl}/trees/${treeId}`)
            .map(
                (response: Response) => {
                    const data = response.json() as Tree;
                    return data;
                }
            );
    }

    deleteTree(treeId: string): Observable<Response> {
        return this.http.delete(`${this.baseUrl}/trees/${treeId}`);
    }

    createTree(treeName: string): Observable<Response> {
        return this.http.post(`${this.baseUrl}/trees`, {
            name: treeName
        })
    }

    updateTree(treeId: string, treeName: string): Observable<Response> {
        return this.http.put(`${this.baseUrl}/trees/${treeId}`, {
            name: treeName
        })
    }
    
}