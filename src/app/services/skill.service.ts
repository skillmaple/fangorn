import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Skill } from '../models/domain/skill';
import { SkillFamily } from '../models/domain/skill-family';

@Injectable()
export class SkillService {
    baseUrl: string;
    
    constructor(private http:Http) {
        this.baseUrl = `https://${window.location.hostname}/api/v1`;
    }

    getSkillFamily(skillId: string): Observable<SkillFamily> {
        return this.http.get(`${this.baseUrl}/skills/${skillId}/family`)
            .map(
                (response: Response) => {
                    const data = response.json() as SkillFamily;
                    return data;
                }
            );
    }

    getSkill(skillId: string): Observable<Skill> {
        return this.http.get(`${this.baseUrl}/skills/${skillId}`)
            .map(
                (response: Response) => {
                    const data = response.json() as Skill;
                    return data;
                }
            );
    }

    updateSkill(skill: Skill): Observable<Response> {
        return this.http.put(`${this.baseUrl}/skills/${skill.skillId}`, {
            name: skill.name,
            icon: skill.icon,
            description: skill.description,
            background: skill.background
        });
    }

    createSubskill(parentId: string, skillName: string): Observable<Response> {
        return this.http.post(`${this.baseUrl}/skills/${parentId}/children`, {
            name: skillName,
            description: '',
            background: ''
        });
    }

    deleteSkill(skillId: string): Observable<Response> {
        return this.http.delete(`${this.baseUrl}/skills/${skillId}`);
    }


    addParent(skillId: string, parentId: string): Observable<Response> {
        return this.http.post(`${this.baseUrl}/skills/${skillId}/parents/${parentId}`, {});
    }

    removeParent(skillId: string, parentId: string): Observable<Response> {
        return this.http.delete(`${this.baseUrl}/skills/${skillId}/parents/${parentId}`);
    }

    
}