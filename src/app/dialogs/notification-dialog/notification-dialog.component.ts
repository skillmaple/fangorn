import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-notification-dialog',
  templateUrl: './notification-dialog.component.html',
  styleUrls: ['./notification-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NotificationDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(MatDialogRef) public dialogRef: MatDialogRef<NotificationDialogComponent>
  ) { }

  ngOnInit() {
  }

  okButton() {
    this.dialogRef.close();
  }

}
