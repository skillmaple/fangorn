import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-parent-dialog',
  templateUrl: './add-parent-dialog.component.html',
  styleUrls: ['./add-parent-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddParentDialogComponent implements OnInit {

  public skillForm: FormGroup;
  
    constructor(
      @Inject(MAT_DIALOG_DATA) public data: any,
      @Inject(MatDialogRef) public dialogRef: MatDialogRef<AddParentDialogComponent>
    ){}
  
    ngOnInit() {
      this.setupForm();
    }
  
    setupForm() {
      this.skillForm = new FormGroup({
        'skillId': new FormControl('', [Validators.required, this.isUUID])
      });
    }
  
    isUUID(control: FormControl): {[s: string]: boolean} {
      if (control.value != null && control.value.length > 80) {
        return {'format': true};
      }
      return null;
    }
  
    submit() {
      if (this.skillForm.valid) {
        this.dialogRef.close(this.skillForm.value.skillId);
      }
    }
  
    cancel() {
      this.dialogRef.close("");
    }

}
