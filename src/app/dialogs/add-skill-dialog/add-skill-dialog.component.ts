import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-skill-dialog',
  templateUrl: './add-skill-dialog.component.html',
  styleUrls: ['./add-skill-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddSkillDialogComponent implements OnInit {

  public skillForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    @Inject(MatDialogRef) public dialogRef: MatDialogRef<AddSkillDialogComponent>
  ){}

  ngOnInit() {
    this.setupForm();
  }

  setupForm() {
    this.skillForm = new FormGroup({
      'skillName': new FormControl('', [Validators.required, this.maxLength])
    });
  }

  maxLength(control: FormControl): {[s: string]: boolean} {
    if (control.value != null && control.value.length > 80) {
      return {'maxLength': true};
    }
    return null;
  }

  submit() {
    if (this.skillForm.valid) {
      this.dialogRef.close(this.skillForm.value.skillName);
    }
  }

  cancel() {
    this.dialogRef.close("");
  }
}
