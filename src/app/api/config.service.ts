import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Injectable } from '@angular/core';
import { Http, Response} from '@angular/http';

@Injectable()
export class ConfigService {
    baseUrl: string;

    constructor(private http:Http) {
        this.baseUrl = `https://${window.location.hostname}/api/v1`;
    }

    getInitConfig(): Observable<any> {
        return this.http.get(`${this.baseUrl}/config/samara`)
            .map(
                (response: Response) => {
                    const data = response.json();
                    return data;
                }
            );
    }
 }