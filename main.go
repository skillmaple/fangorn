package main

import (
	"net/http"
	"os"
	"regexp"
)

func isFileRequest(r *http.Request) bool {
	path := r.URL.Path
	regex, err := regexp.Compile("(.*\\.js|.*\\.css|.*\\.html|.*\\.js\\.map|.*\\.ico)$")
	if err != nil {
		// TODO: Log the error
		return true
	}
	return regex.MatchString(path)
}

func customHandler(fileHandler http.Handler, indexHandler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if isFileRequest(r) {
			fileHandler.ServeHTTP(w, r)
		} else {
			indexHandler.ServeHTTP(w, r)
		}
	})
}

func indexHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "dist/admin/index.html")
	})
}

func main() {
	homePort := ":" + os.Getenv("FANGORN_SERVICE_LISTENER")
	if homePort == ":" {
		homePort = ":80"
	}
	fs := http.FileServer(http.Dir("dist"))
	mux := http.NewServeMux()
	mux.Handle("/", customHandler(fs, indexHandler()))
	http.ListenAndServe(homePort, mux)
}
